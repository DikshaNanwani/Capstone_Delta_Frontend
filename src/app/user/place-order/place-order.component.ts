import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.scss']
})
export class PlaceOrderComponent implements OnInit {

  constructor(private dataservice:DataService,private formbuilder:FormBuilder,private toastr: ToastrService,private api:ApiService,private router:Router) { }
  item:any;
  quantity=1;
  price:any;
  user:any;
  login=false;
  ngOnInit(): void {
    this.item =this.dataservice.getObject();
    this.price=this.item.productPrice;
    console.log(this.item)
    this.user = JSON.parse(localStorage.getItem('User')||"{}");
    // this.user = this.dataservice.getUser();
    // console.log("user",this.user)
    if(this.user.loginRole)
     {
      this.login=true      
     }else{
      this.login=false;
     }
  }

  myForm: FormGroup = this.formbuilder.group({
    cardNumber: ["", [Validators.required]],
    cardExpire:["", [Validators.required]],
    cardcvv:["", [Validators.required]],


  
  })

  quanchange()
  {
    this.price = this.item.productPrice*this.quantity;
  }

  placeOrder()
  {
    if(this.login == true)
    {
      if(this.myForm.status=='VALID')
    {
      //create order
      
      let createorderurl=`https://localhost:44337/api/order/${this.item.productId}/${this.user.customerId}/${this.quantity}/${this.price}/${this.user.address}`
      this.api.post(createorderurl).subscribe(res=>{
        console.log("Order created",res)
        let orderRes = JSON.parse(JSON.stringify(res));
        let paymenturl = `https://localhost:44337/api/addpayment/${orderRes.orderId}/${this.user.customerId}/Card/${this.myForm.value.cardNumber}/${this.myForm.value.cardcvv}/${this.myForm.value.cardExpire}/MasterVisa`;
        this.api.post(paymenturl).subscribe(paymentresponse=>{
          console.log("payment",paymentresponse)
          this.toastr.success("Order Placed")
          this.router.navigateByUrl('/my-orders');

        })
      })
    }else{
      this.toastr.error("Enter Card Details")
    }
    }else{
      this.toastr.info("Please Login First")
      this.router.navigateByUrl('/login');
    }

    
  }

}

